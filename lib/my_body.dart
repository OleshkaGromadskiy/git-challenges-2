import 'package:flutter/material.dart';
import 'package:udemy_course_task_13/Dialog.dart';

class MyBody extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dialog"),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: FlatButton(
          onPressed: () => DialogHelper.instance.show(context),
          child: FittedBox(
            fit: BoxFit.cover,
            child: Icon(
              Icons.do_not_touch_rounded,
              size: 100,
            ),
          ),
        ),
      ),
    );
  }
}
