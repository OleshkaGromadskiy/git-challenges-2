import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class Dialog {
  Widget _widget;

  void show(BuildContext context);
}

class DefaultErrorDialog implements Dialog {
  @override
  Widget _widget = AlertDialog(
    content: Text("Do you realy want to click!?!?!?"),
    actions: [
      FlatButton(
        child: Text("Yes"),
        onPressed: () {},
      ),
      FlatButton(
        child: Text("Yes"),
        onPressed: () {},
      ),
    ],
  );

  @override
  void show(BuildContext context) {
    showDialog(context: context, builder: (ctx) => _widget);
  }

}

class DialogHelper {
  static DefaultErrorDialog instance = new DefaultErrorDialog();
}
