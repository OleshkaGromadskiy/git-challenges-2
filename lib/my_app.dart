import 'package:flutter/material.dart';
import 'package:udemy_course_task_13/my_body.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: MyBody(),
    );
  }
}
